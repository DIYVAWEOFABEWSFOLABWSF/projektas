-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: klausimynas_mislynas
-- ------------------------------------------------------
-- Server version	5.7.19-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `misles`
--
create database klausimynas_mislynas;
use klausimynas_mislynas;


DROP TABLE IF EXISTS `misles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `misles` (
  `ID` int(6) NOT NULL AUTO_INCREMENT,
  `tipoID` int(6) NOT NULL,
  `misle` text NOT NULL,
  `atsakymas` text,
  `fakeatsakymas` text,
  PRIMARY KEY (`ID`),
  KEY `tipoID` (`tipoID`),
  CONSTRAINT `misles_ibfk_1` FOREIGN KEY (`tipoID`) REFERENCES `misliu_tipai` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=110 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `misles`
--

LOCK TABLES `misles` WRITE;
/*!40000 ALTER TABLE `misles` DISABLE KEYS */;
INSERT INTO `misles` VALUES (10,2,'Ant kojytes kepuryte','',''),(11,2,'Ant kojytes kepuryte','Grybas','Medis'),(12,2,'Ant vieno kotelio daug veliaveliu zaidzia','Grybas','Medis'),(13,2,'Ant vieno kotelio daug veliaveliu zaidzia','Medis','Grybas'),(14,2,'Apskritas kabo, o barzda dreba','Medis','Grybas'),(15,2,'Apskritas kabo, o barzda dreba','Obuolys','Klevas'),(16,2,'Ateina bobele su devyniom skarelem, kas ja paliecia, tas gailiai verkia','Obuolys','Klevas'),(17,2,'Ateina bobele su devyniom skarelem, kas ja paliecia, tas gailiai verkia','Svogunas','Morka'),(18,2,'Auga azuolelis, ant virsunes obuolelis','Svogunas','Morka'),(19,2,'Auga azuolelis, ant virsunes obuolelis','Linas','Aguona'),(20,2,'Auksta lazdele, didele galvele','Linas','Aguona'),(21,2,'Auksta lazdele, didele galvele','Aguona','Linas'),(22,2,'Aukstas senis, balta oda, medziams sprogstant pieno duoda','Aguona','Linas'),(23,2,'Aukstas senis, balta oda, medziams sprogstant pieno duoda','Berzas','Zole'),(24,2,'Balta kaip sniegas, zalia kaip dobilas, raudona kaip kraujas','Berzas','Zole'),(25,2,'Balta kaip sniegas, zalia kaip dobilas, raudona kaip kraujas','Vysnia','Braske'),(26,2,'Be duru, be lango pilna troba sveciu','Vysnia','Braske'),(27,2,'Be duru, be lango pilna troba sveciu','Agurkas','Morka'),(28,2,'Ezys apredytas, asloj pastatytas','Agurkas','Morka'),(29,2,'Ezys apredytas, asloj pastatytas','Egle','Azuolas'),(30,5,'Atleke paukstis be sparnu, ikando zmogui be dantu','Egle','Azuolas'),(31,5,'Atleke paukstis be sparnu, ikando zmogui be dantu','Kulka','Akmuo'),(32,5,'Apskritas, pailgas, visiems reikalingas','Kulka','Akmuo'),(33,5,'Apskritas, pailgas, visiems reikalingas','Pinigas','Knyga'),(34,5,'Auksta panele, zalia suknele, per viduri susijuosus po troba sokineja','Pinigas','Knyga'),(35,5,'Auksta panele, zalia suknele, per viduri susijuosus po troba sokineja','sluota','Sautuvas'),(36,5,'Ant misko siulo tinklas','sluota','Sautuvas'),(37,5,'Ant misko siulo tinklas','Kepure','Kaspinas'),(38,5,'Ant auksto kalno be liezuvio bliauna','Kepure','Kaspinas'),(39,5,'Ant auksto kalno be liezuvio bliauna','Varpas','Duda'),(40,5,'Be akio, o neregius vedzioja','Varpas','Duda'),(41,5,'Be akio, o neregius vedzioja','Lazda','Akiniai'),(42,5,'Be pradzios, be pabaigos, bet ne Dievas','Lazda','Akiniai'),(43,5,'Be pradzios, be pabaigos, bet ne Dievas','Ratas','Vezimas'),(44,5,'Du galai astriai nusmailinti ir abu prieki nudailinti','Ratas','Vezimas'),(45,5,'Du galai astriai nusmailinti ir abu prieki nudailinti','zirkles','Akmuo'),(46,5,'Didelis paukstis storu balsu kurkia','zirkles','Akmuo'),(47,5,'Didelis paukstis storu balsu kurkia','Lektuvas','Masina'),(48,5,'Daug vaiku rankomis susikibe','Lektuvas','Masina'),(49,5,'Daug vaiku rankomis susikibe','Tvora','Sienas'),(50,4,'Kas vakara mirsta, o ryta gimsta','Tvora','Sienas'),(51,4,'Kas vakara mirsta, o ryta gimsta','Diena','Naktis'),(52,4,'Kai pridedi � sumazeja, kai atimi � padideja','Diena','Naktis'),(53,4,'Kai pridedi � sumazeja, kai atimi � padideja','Duobe','Vanduo'),(54,4,'Jaunas zaliuoja, pasen�s zemen griuva, mirdamas dangun lekia','Duobe','Vanduo'),(55,4,'Jaunas zaliuoja, pasen�s zemen griuva, mirdamas dangun lekia','Medis','Zeme'),(56,4,'Uzaugo ant lauko be saknu?','Medis','Zeme'),(57,4,'Uzaugo ant lauko be saknu?','Akmuo','Zmogus'),(58,4,'Auksu mesta, sidabru austa, deimanto peiliu rezta. ','Akmuo','Zmogus'),(59,4,'Auksu mesta, sidabru austa, deimanto peiliu rezta. ','Vaivorykste','Juosta'),(60,4,'Be koju, be ranku vartus atkelia','Vaivorykste','Juosta'),(61,4,'Be koju, be ranku vartus atkelia','Vejas','Arklys'),(62,4,'Be ranku, be koju tilta pastato','Vejas','Arklys'),(63,4,'Be ranku, be koju tilta pastato','Ledas','Vejas'),(64,4,'cia yra, cia nera, bet niekuomet nezuna','Ledas','Vejas'),(65,4,'cia yra, cia nera, bet niekuomet nezuna','Menulis','kaktusas'),(66,4,'Dega visa diena, bet nesudega','Menulis','kaktusas'),(67,4,'Dega visa diena, bet nesudega','Saule','menulis'),(68,4,'Juoda karve visus gyvius pargriove','Saule','menulis'),(69,4,'Juoda karve visus gyvius pargriove','Naktis','Lyronas'),(70,3,'Ant lenteliu vaikstin�ja, ragu zole skabineja','Naktis','Lyronas'),(71,3,'Ant lenteliu vaikstin�ja, ragu zole skabineja','zasis','Zmogus'),(72,3,'Ateina bajoras ant dvieju kriukiu, su mesos barzda, su kaulo burna','zasis','Zmogus'),(73,3,'Ateina bajoras ant dvieju kriukiu, su mesos barzda, su kaulo burna','Gaidys','Vista'),(74,3,'Ateis menuo mojaus, atleks paukstis is gojaus, kas ta pauksti uzmus � savo krauja pralies','Gaidys','Vista'),(75,3,'Ateis menuo mojaus, atleks paukstis is gojaus, kas ta pauksti uzmus � savo krauja pralies','Uodas','Muse'),(76,3,'Audzia be stakliu','Uodas','Muse'),(77,3,'Audzia be stakliu','Voras','Vytute'),(78,3,'Auksta bajore, po kaklu kasa','Voras','Vytute'),(79,3,'Auksta bajore, po kaklu kasa','Ozka','Gegute'),(80,3,'Be ranku, o namus pastato','Ozka','Gegute'),(81,3,'Be ranku, o namus pastato','Paukstis','maldininkas'),(82,3,'vyrukas vyrukas ore verkia','Paukstis','maldininkas'),(83,3,'vyrukas vyrukas ore verkia','Vyturys','baznycia'),(84,3,'Du bega, du veja, sesi simtai svilpia','Vyturys','baznycia'),(85,3,'Du bega, du veja, sesi simtai svilpia','Arklys','ozka'),(86,3,'Eina be koju, ragai galvoje','Arklys','ozka'),(87,3,'Eina be koju, ragai galvoje','Sraige','skruzde'),(88,3,'Eina mesos zmotas, viniu priklotas','Sraige','skruzde'),(89,3,'Eina mesos zmotas, viniu priklotas','Ezys','kiskis'),(90,1,'Ateina tyledama, iseina giedodama','Ezys','kiskis'),(91,1,'Ateina tyledama, iseina giedodama','Mirtis','diena'),(92,1,'Ateina protingas, iseina kvailas','Mirtis','diena'),(93,1,'Ateina protingas, iseina kvailas','Girtuoklis','absolventas'),(94,1,'Baltu visteliu pilna laktele','Girtuoklis','absolventas'),(95,1,'Baltu visteliu pilna laktele','Dantys','ausys'),(96,1,'Dvi seseles per kalneli nesueina','Dantys','ausys'),(97,1,'Dvi seseles per kalneli nesueina','Akys','dantys'),(98,1,'Du ratai pagiry stovi','Akys','dantys'),(99,1,'Du ratai pagiry stovi','Ausys','akys'),(100,1,'Dvi mergaites viska apsviecia','Ausys','akys'),(101,1,'Dvi mergaites viska apsviecia','Akys','dantys'),(102,1,'Dvejiems vartams atsiverus, arklys zvengia','Akys','dantys'),(103,1,'Dvejiems vartams atsiverus, arklys zvengia','Liezuvis','nosis'),(104,1,'eda kaip arklys, dirba kaip gaidys','Liezuvis','nosis'),(105,1,'eda kaip arklys, dirba kaip gaidys','Tinginys','Gonzalesas'),(106,1,'Gardus, saldus, leksten nepadedamas','Tinginys','Gonzalesas'),(107,1,'Gardus, saldus, leksten nepadedamas','Miegas','KFC'),(108,1,'Gyvena be k�no, kalba be liezuvio, visi ji girdi, bet niekas nemato','Miegas','KFC'),(109,1,'Gyvena be k�no, kalba be liezuvio, visi ji girdi, bet niekas nemato','Aidas','ekonomika');
/*!40000 ALTER TABLE `misles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `misliu_tipai`
--

DROP TABLE IF EXISTS `misliu_tipai`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `misliu_tipai` (
  `ID` int(6) NOT NULL AUTO_INCREMENT,
  `tipopav` varchar(60) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `misliu_tipai`
--

LOCK TABLES `misliu_tipai` WRITE;
/*!40000 ALTER TABLE `misliu_tipai` DISABLE KEYS */;
INSERT INTO `misliu_tipai` VALUES (1,'apie_augalus'),(2,'apie_daiktus'),(3,'apie_gamta'),(4,'apie_gyvunusIrVabzdzius'),(5,'apie_zmones');
/*!40000 ALTER TABLE `misliu_tipai` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `ID` int(6) NOT NULL AUTO_INCREMENT,
  `username` varchar(60) NOT NULL,
  `pass` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-01-07 10:21:40

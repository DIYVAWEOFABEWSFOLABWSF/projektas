﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace klausimynas_mislynas
{
    public class Misle
    {
        public string misle { get; set; }
        public string atsakymas { get; set; }
        public string fakeatsakymas { get; set; }
        public int tipoID { get; set; }
    }
}

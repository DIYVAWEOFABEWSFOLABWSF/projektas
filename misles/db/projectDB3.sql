CREATE DATABASE  IF NOT EXISTS `klausimynas_mislynas` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `klausimynas_mislynas`;
-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: klausimynas_mislynas
-- ------------------------------------------------------
-- Server version	5.7.19-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `misles`
--

DROP TABLE IF EXISTS `misles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `misles` (
  `ID` int(6) NOT NULL AUTO_INCREMENT,
  `tipoID` int(6) NOT NULL,
  `misle` text NOT NULL,
  `atsakymas` text,
  `fakeatsakymas` text,
  PRIMARY KEY (`ID`),
  KEY `tipoID` (`tipoID`),
  CONSTRAINT `misles_ibfk_1` FOREIGN KEY (`tipoID`) REFERENCES `misliu_tipai` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=232 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `misles`
--

LOCK TABLES `misles` WRITE;
/*!40000 ALTER TABLE `misles` DISABLE KEYS */;
INSERT INTO `misles` VALUES (182,1,'Ant kojytes kepuryte','',''),(183,1,'Ant vieno kotelio daug veliaveliu zaidzia','Grybas','Medis'),(184,1,'Apskritas kabo, o barzda dreba','Medis','Grybas'),(185,1,'Ateina bobele su devyniom skarelem, kas ja paliecia, tas gailiai verkia','Obuolys','Klevas'),(186,1,'Auga azuolelis, ant virsunes obuolelis','Svogunas','Morka'),(187,1,'Auksta lazdele, didele galvele','Linas','Aguona'),(188,1,'Aukstas senis, balta oda, medziams sprogstant pieno duoda','Aguona','Linas'),(189,1,'Balta kaip sniegas, zalia kaip dobilas, raudona kaip kraujas','Berzas','Zole'),(190,1,'Be duru, be lango pilna troba sveciu','Vysnia','Braske'),(191,1,'Ezys apredytas, asloj pastatytas','Agurkas','Morka'),(192,2,'Atleke paukstis be sparnu, ikando zmogui be dantu','Egle','Azuolas'),(193,2,'Apskritas, pailgas, visiems reikalingas','Kulka','Akmuo'),(194,2,'Auksta panele, zalia suknele, per viduri susijuosus po troba sokineja','Pinigas','Knyga'),(195,2,'Ant misko siulo tinklas','sluota','Sautuvas'),(196,2,'Ant auksto kalno be liezuvio bliauna','Kepure','Kaspinas'),(197,2,'Be akio, o neregius vedzioja','Varpas','Duda'),(198,2,'Be pradzios, be pabaigos, bet ne Dievas','Lazda','Akiniai'),(199,2,'Du galai astriai nusmailinti ir abu prieki nudailinti','Ratas','Vezimas'),(200,2,'Didelis paukstis storu balsu kurkia','zirkles','Akmuo'),(201,2,'Daug vaiku rankomis susikibe','Lektuvas','Masina'),(202,4,'Ant kojytes kepuryte','Tvora','Sienas'),(203,4,'Ant vieno kotelio daug veliaveliu zaidzia','zasis','Zmogus'),(204,4,'Apskritas kabo, o barzda dreba','Gaidys','Vista'),(205,4,'Ateina bobele su devyniom skarelem, kas ja paliecia, tas gailiai verkia','Uodas','Muse'),(206,4,'Auga azuolelis, ant virsunes obuolelis','Voras','Vytute'),(207,4,'Auksta lazdele, didele galvele','Ozka','Gegute'),(208,4,'Aukstas senis, balta oda, medziams sprogstant pieno duoda','Paukstis','maldininkas'),(209,4,'Balta kaip sniegas, zalia kaip dobilas, raudona kaip kraujas','Vyturys','baznycia'),(210,4,'Be duru, be lango pilna troba sveciu','Arklys','ozka'),(211,4,'Ezys apredytas, asloj pastatytas','Sraige','skruzde'),(212,4,'Ant lenteliu vaikstin�ja, ragu zole skabineja','Ezys','kiskis'),(213,4,'Ateina bajoras ant dvieju kriukiu, su mesos barzda, su kaulo burna','zasis','Zmogus'),(214,4,'Ateis menuo mojaus, atleks paukstis is gojaus, kas ta pauksti uzmus � savo krauja pralies','Gaidys','Vista'),(215,4,'Audzia be stakliu','Uodas','Muse'),(216,4,'Auksta bajore, po kaklu kasa','Voras','Vytute'),(217,4,'Be ranku, o namus pastato','Ozka','Gegute'),(218,4,'vyrukas vyrukas ore verkia','Paukstis','maldininkas'),(219,4,'Du bega, du veja, sesi simtai svilpia','Vyturys','baznycia'),(220,4,'Eina be koju, ragai galvoje','Arklys','ozka'),(221,4,'Eina mesos zmotas, viniu priklotas','Sraige','skruzde'),(222,3,'Kas vakara mirsta, o ryta gimsta','Ezys','kiskis'),(223,3,'Kai pridedi � sumazeja, kai atimi � padideja','Diena','Naktis'),(224,3,'Jaunas zaliuoja, pasen�s zemen griuva, mirdamas dangun lekia','Duobe','Vanduo'),(225,3,'Uzaugo ant lauko be saknu?','Medis','Zeme'),(226,3,'Auksu mesta, sidabru austa, deimanto peiliu rezta. ','Akmuo','Zmogus'),(227,3,'Be koju, be ranku vartus atkelia','Vaivorykste','Juosta'),(228,3,'Be ranku, be koju tilta pastato','Vejas','Arklys'),(229,3,'cia yra, cia nera, bet niekuomet nezuna','Ledas','Vejas'),(230,3,'Dega visa diena, bet nesudega','Menulis','kaktusas'),(231,3,'Juoda karve visus gyvius pargriove','Saule','menulis');
/*!40000 ALTER TABLE `misles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `misliu_tipai`
--

DROP TABLE IF EXISTS `misliu_tipai`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `misliu_tipai` (
  `ID` int(6) NOT NULL AUTO_INCREMENT,
  `tipopav` varchar(60) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `misliu_tipai`
--

LOCK TABLES `misliu_tipai` WRITE;
/*!40000 ALTER TABLE `misliu_tipai` DISABLE KEYS */;
INSERT INTO `misliu_tipai` VALUES (1,'apie_augalus'),(2,'apie_daiktus'),(3,'apie_gamta'),(4,'apie_gyvunusIrVabzdzius'),(5,'apie_zmones');
/*!40000 ALTER TABLE `misliu_tipai` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `ID` int(6) NOT NULL AUTO_INCREMENT,
  `username` varchar(60) NOT NULL,
  `pass` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'klausimynas_mislynas'
--

--
-- Dumping routines for database 'klausimynas_mislynas'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-01-23 19:17:39

-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: klausimynas_mislynas
-- ------------------------------------------------------
-- Server version	5.7.19-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `misles`
--

DROP TABLE IF EXISTS `misles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `misles` (
  `ID` int(6) NOT NULL AUTO_INCREMENT,
  `tipoID` int(6) NOT NULL,
  `misle` text NOT NULL,
  `atsakymas` text,
  `fakeatsakymas` text,
  PRIMARY KEY (`ID`),
  KEY `tipoID` (`tipoID`),
  CONSTRAINT `misles_ibfk_1` FOREIGN KEY (`tipoID`) REFERENCES `misliu_tipai` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=225 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `misles`
--

LOCK TABLES `misles` WRITE;
/*!40000 ALTER TABLE `misles` DISABLE KEYS */;
INSERT INTO `misles` VALUES (125,2,'Ant kojyt�s kepuryt�','',''),(126,2,'Ant kojyt�s kepuryt�','Grybas','Medis'),(127,2,'Ant vieno kotelio daug v�liav�li� zaidzia','Grybas','Medis'),(128,2,'Ant vieno kotelio daug v�liav�li� zaidzia','Medis','Grybas'),(129,2,'Apskritas kabo, o barzda dreba','Medis','Grybas'),(130,2,'Apskritas kabo, o barzda dreba','Obuolys','Klevas'),(131,2,'Ateina bobel� su devyniom skarel�m, kas j� palie�ia, tas gailiai verkia','Obuolys','Klevas'),(132,2,'Ateina bobel� su devyniom skarel�m, kas j� palie�ia, tas gailiai verkia','Svog�nas','Morka'),(133,2,'Auga ��uol�lis, ant vir��n�s obuol�lis','Svog�nas','Morka'),(134,2,'Auga ��uol�lis, ant vir��n�s obuol�lis','Linas','Aguona'),(135,2,'Auk�ta lazdel�, didel� galvel�','Linas','Aguona'),(136,2,'Auk�ta lazdel�, didel� galvel�','Aguona','Linas'),(137,2,'Auk�tas senis, balta oda, med�iams sprogstant pieno duoda','Aguona','Linas'),(138,2,'Auk�tas senis, balta oda, med�iams sprogstant pieno duoda','Ber�as','Zole'),(139,2,'Balta kaip sniegas, �alia kaip dobilas, raudona kaip kraujas','Ber�as','Zole'),(140,2,'Balta kaip sniegas, �alia kaip dobilas, raudona kaip kraujas','Vy�nia','Braske'),(141,2,'Be dur�, be lang� pilna troba sve�i�','Vy�nia','Braske'),(142,2,'Be dur�, be lang� pilna troba sve�i�','Agurkas','Morka'),(143,2,'E�ys apr�dytas, asloj pastatytas','Agurkas','Morka'),(144,2,'E�ys apr�dytas, asloj pastatytas','Egl�','Azuolas'),(145,5,'Atl�k� pauk�tis be sparn�, �kando �mogui be dant�','Egl�','Azuolas'),(146,5,'Atl�k� pauk�tis be sparn�, �kando �mogui be dant�','Kulka','Akmuo'),(147,5,'Apskritas, pailgas, visiems reikalingas','Kulka','Akmuo'),(148,5,'Apskritas, pailgas, visiems reikalingas','Pinigas','Knyga'),(149,5,'Auk�ta panel�, �alia suknel�, per vidur� susijuosus po trob� �okin�ja','Pinigas','Knyga'),(150,5,'Auk�ta panel�, �alia suknel�, per vidur� susijuosus po trob� �okin�ja','�luota','Sautuvas'),(151,5,'Ant mi�ko si�l� tinklas','�luota','Sautuvas'),(152,5,'Ant mi�ko si�l� tinklas','Kepur�','Kaspinas'),(153,5,'Ant auk�to kalno be lie�uvio bliauna','Kepur�','Kaspinas'),(154,5,'Ant auk�to kalno be lie�uvio bliauna','Varpas','Duda'),(155,5,'Be aki�, o neregius ved�ioja','Varpas','Duda'),(156,5,'Be aki�, o neregius ved�ioja','Lazda','Akiniai'),(157,5,'Be prad�ios, be pabaigos, bet ne Dievas','Lazda','Akiniai'),(158,5,'Be prad�ios, be pabaigos, bet ne Dievas','Ratas','Vezimas'),(159,5,'Du galai a�triai nusmailinti ir abu � �iedus nudailinti','Ratas','Vezimas'),(160,5,'Du galai a�triai nusmailinti ir abu � �iedus nudailinti','�irkl�s','Akmuo'),(161,5,'Didelis pauk�tis storu balsu kurkia','�irkl�s','Akmuo'),(162,5,'Didelis pauk�tis storu balsu kurkia','L�ktuvas','Masina'),(163,5,'Daug vaik� rankomis susikib�','L�ktuvas','Masina'),(164,5,'Daug vaik� rankomis susikib�','Tvora','Sienas'),(165,4,'Kas vakar� mir�ta, o ryt� gimsta','Tvora','Sienas'),(166,4,'Kas vakar� mir�ta, o ryt� gimsta','Diena','Naktis'),(167,4,'Kai pridedi � suma��ja, kai atimi � padid�ja','Diena','Naktis'),(168,4,'Kai pridedi � suma��ja, kai atimi � padid�ja','Duob�','Vanduo'),(169,4,'Jaunas �aliuoja, pasen�s �em�n gri�va, mirdamas dangun lekia','Duob�','Vanduo'),(170,4,'Jaunas �aliuoja, pasen�s �em�n gri�va, mirdamas dangun lekia','Medis','Zeme'),(171,4,'U�augo ant lauko be �akn�?','Medis','Zeme'),(172,4,'U�augo ant lauko be �akn�?','Akmuo','Zmogus'),(173,4,'Auksu mesta, sidabru austa, deimanto peiliu r��ta. ','Akmuo','Zmogus'),(174,4,'Auksu mesta, sidabru austa, deimanto peiliu r��ta. ','Vaivoryk�t�','Juosta'),(175,4,'Be koj�, be rank� vartus atkelia','Vaivoryk�t�','Juosta'),(176,4,'Be koj�, be rank� vartus atkelia','V�jas','Arklys'),(177,4,'Be rank�, be koj� tilt� pastato','V�jas','Arklys'),(178,4,'Be rank�, be koj� tilt� pastato','Ledas','Vejas'),(179,4,'�ia yra, �ia n�ra, bet niekuomet ne��na','Ledas','Vejas'),(180,4,'�ia yra, �ia n�ra, bet niekuomet ne��na','M�nulis','kaktusas'),(181,4,'Dega vis� dien�, bet nesudega','M�nulis','kaktusas'),(182,4,'Dega vis� dien�, bet nesudega','Saul�','menulis'),(183,4,'Juoda karv� visus gyvius pargriov�','Saul�','menulis'),(184,4,'Juoda karv� visus gyvius pargriov�','Naktis','Lyronas'),(185,3,'Ant lenteli� vaik�tin�ja, ragu �ol� skabin�ja','Naktis','Lyronas'),(186,3,'Ant lenteli� vaik�tin�ja, ragu �ol� skabin�ja','��sis','Zmogus'),(187,3,'Ateina bajoras ant dviej� kriuki�, su m�sos barzda, su kaulo burna','��sis','Zmogus'),(188,3,'Ateina bajoras ant dviej� kriuki�, su m�sos barzda, su kaulo burna','Gaidys','Vista'),(189,3,'Ateis m�nuo mojaus, atl�ks pauk�tis i� gojaus, kas t� pauk�t� u�mu� � savo krauj� pralies','Gaidys','Vista'),(190,3,'Ateis m�nuo mojaus, atl�ks pauk�tis i� gojaus, kas t� pauk�t� u�mu� � savo krauj� pralies','Uodas','Muse'),(191,3,'Aud�ia be stakli�','Uodas','Muse'),(192,3,'Aud�ia be stakli�','Voras','Vytute'),(193,3,'Auk�ta bajor�, po kaklu kasa','Voras','Vytute'),(194,3,'Auk�ta bajor�, po kaklu kasa','O�ka','Gegute'),(195,3,'Be rank�, o namus pastato','O�ka','Gegute'),(196,3,'Be rank�, o namus pastato','Pauk�tis','maldininkas'),(197,3,'�yrukas vyrukas ore verkia','Pauk�tis','maldininkas'),(198,3,'�yrukas vyrukas ore verkia','Vyturys','baznycia'),(199,3,'Du b�ga, du veja, �e�i �imtai �vilpia','Vyturys','baznycia'),(200,3,'Du b�ga, du veja, �e�i �imtai �vilpia','Arklys','ozka'),(201,3,'Eina be koj�, ragai galvoje','Arklys','ozka'),(202,3,'Eina be koj�, ragai galvoje','Sraig�','skruzde'),(203,3,'Eina m�sos �motas, vini� priklotas','Sraig�','skruzde'),(204,3,'Eina m�sos �motas, vini� priklotas','E�ys','kiskis'),(205,1,'Ateina tyl�dama, i�eina giedodama','E�ys','kiskis'),(206,1,'Ateina tyl�dama, i�eina giedodama','Mirtis','diena'),(207,1,'Ateina protingas, i�eina kvailas','Mirtis','diena'),(208,1,'Ateina protingas, i�eina kvailas','Girtuoklis','absolventas'),(209,1,'Balt� vi�teli� pilna laktel�','Girtuoklis','absolventas'),(210,1,'Balt� vi�teli� pilna laktel�','Dantys','ausys'),(211,1,'Dvi sesel�s per kalnel� nesueina','Dantys','ausys'),(212,1,'Dvi sesel�s per kalnel� nesueina','Akys','dantys'),(213,1,'Du ratai pagiry stovi','Akys','dantys'),(214,1,'Du ratai pagiry stovi','Ausys','akys'),(215,1,'Dvi mergait�s visk� ap�vie�ia','Ausys','akys'),(216,1,'Dvi mergait�s visk� ap�vie�ia','Akys','dantys'),(217,1,'Dvejiems vartams atsiv�rus, arklys �vengia','Akys','dantys'),(218,1,'Dvejiems vartams atsiv�rus, arklys �vengia','Lie�uvis','nosis'),(219,1,'�da kaip arklys, dirba kaip gaidys','Lie�uvis','nosis'),(220,1,'�da kaip arklys, dirba kaip gaidys','Tinginys','Gonzalesas'),(221,1,'Gardus, saldus, l�k�t�n nepadedamas','Tinginys','Gonzalesas'),(222,1,'Gardus, saldus, l�k�t�n nepadedamas','Miegas','KFC'),(223,1,'Gyvena be k�no, kalba be lie�uvio, visi j� girdi, bet niekas nemato','Miegas','KFC'),(224,1,'Gyvena be k�no, kalba be lie�uvio, visi j� girdi, bet niekas nemato','Aidas','ekonomika');
/*!40000 ALTER TABLE `misles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `misliu_tipai`
--

DROP TABLE IF EXISTS `misliu_tipai`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `misliu_tipai` (
  `ID` int(6) NOT NULL AUTO_INCREMENT,
  `tipopav` varchar(60) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `misliu_tipai`
--

LOCK TABLES `misliu_tipai` WRITE;
/*!40000 ALTER TABLE `misliu_tipai` DISABLE KEYS */;
INSERT INTO `misliu_tipai` VALUES (1,'apie_augalus'),(2,'apie_daiktus'),(3,'apie_gamta'),(4,'apie_gyvunusIrVabzdzius'),(5,'apie_zmones');
/*!40000 ALTER TABLE `misliu_tipai` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `ID` int(6) NOT NULL AUTO_INCREMENT,
  `username` varchar(60) NOT NULL,
  `pass` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-12-17 10:18:10
